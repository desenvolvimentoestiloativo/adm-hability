export const environment = {
  production: true,
  baseUrl: 'https://srv-app-backend-thy564kwza-rj.a.run.app',
  altHability: 'https://bff-app-backoffice-thy564kwza-rj.a.run.app',
  apiPython: 'https://bff-python-hability-h6247rlihq-rj.a.run.app'
};
