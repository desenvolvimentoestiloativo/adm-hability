import { Route } from '@angular/router';
import { NoAuthGuard } from './core/auth/guards/noAuth.guard';
import { LayoutComponent } from './layout/layout.component';

export const appRoutes: Route[] = [

  {path: '', pathMatch : 'full', redirectTo: 'lista-grupos-adm'},

  // Auth routes (guest)
  {
    path: '',
    canActivate: [NoAuthGuard],
    canActivateChild: [NoAuthGuard],
    component: LayoutComponent,
    data: {
      layout: 'empty'
    },
    children: [
      { path: 'sing-in', loadChildren: () => import('./modules/auth/sign-in/sign-in.module').then(m=> m.AuthSignInModule) }
    ]
  },

  // Auth routes (logged in)
  {
    path: '',
    canActivate: [NoAuthGuard],
    canActivateChild: [NoAuthGuard],
    component: LayoutComponent,
    data: {
        layout: 'vertical'
    },
    children: [
        {path: 'lista-grupos-adm', loadChildren: () => import('./modules/adm/pages/group-adms/group-adms.module').then(m => m.GroupAdmsModule)},
    ]
  },

  // Landing routes
  {
    path: '',
    component  : LayoutComponent,
    data: {
        layout: 'empty'
    },
    children   : [
        {path: 'home', loadChildren: () => import('./modules/landing/home/home.modules').then(m => m.LandingHomeModule)},
    ]
  },



]
