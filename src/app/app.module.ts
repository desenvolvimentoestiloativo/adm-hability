import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { appRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { ExtraOptions, PreloadAllModules, RouterModule } from '@angular/router';
import { CoreModule } from './core/core.module';
import { LayoutModule } from './layout/layout.module';

const routerConfig: ExtraOptions = {
  enableTracing: false, // Quando verdadeiro, registra todos os eventos de navegação interna no console. Use para depuração.
  scrollPositionRestoration: 'enabled',
  preloadingStrategy       : PreloadAllModules,
  relativeLinkResolution   : 'legacy'
}
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, routerConfig),
    // Core
    CoreModule,
    // Layout
    LayoutModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
