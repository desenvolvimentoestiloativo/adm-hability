import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { VerticalLayoutComponent } from './vertical.component';
import { FooterModule } from '../../common/footer/footer.module';
import { SidebarComponent } from '../../common/sidebar/sidebar.component';
import { RightSidebarComponent } from '../../common/right-sidebar/right-sidebar.component';
import { PageMainHeaderComponent } from '../../common/page-main-header/page-main-header.component';

@NgModule({
    declarations: [
        VerticalLayoutComponent,
        SidebarComponent,
        RightSidebarComponent,
        PageMainHeaderComponent,
    ],
    imports     : [
        RouterModule,
        SharedModule,
        FooterModule
    ],
    exports: [
      VerticalLayoutComponent
    ]
})
export class VerticalLayoutModule
{
}
