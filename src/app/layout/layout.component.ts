import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { Layout } from './layout.types';

@Component({
  selector: 'layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LayoutComponent implements OnInit, OnDestroy {

  layout: Layout | undefined;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _activatedRoute: ActivatedRoute,
    @Inject(DOCUMENT) private _document: any,
    private _router: Router
  )
  {
      // Set the private defaults
      this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    let route = this._activatedRoute;
    const layoutFromQueryParam = (route.snapshot.data.layout as Layout);

    if ( layoutFromQueryParam )
    {
        this.layout = layoutFromQueryParam;
    }

    // this._router.events.pipe(
    //   filter(event => event instanceof NavigationEnd),
    //   takeUntil(this._unsubscribeAll)
    // ).subscribe(() => {

    //     console.log('TTESTE')

    //     // Update the layout
    //     // this._updateLayout();
    // });
  }

  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

  private _updateLayout(): void
  {
      // Get the current activated route
      let route = this._activatedRoute;
      while ( route.firstChild )
      {
          route = route.firstChild;
      }
      // 2. Get the query parameter from the current route and
      // set the layout and save the layout to the config
      const layoutFromQueryParam = (route.snapshot.queryParamMap.get('layout') as Layout);

      if ( layoutFromQueryParam )
      {
          this.layout = layoutFromQueryParam;
      }

      // 3. Iterate through the paths and change the layout as we find
      // a config for it.
      //
      // The reason we do this is that there might be empty grouping
      // paths or componentless routes along the path. Because of that,
      // we cannot just assume that the layout configuration will be
      // in the last path's config or in the first path's config.
      //
      // So, we get all the paths that matched starting from root all
      // the way to the current activated route, walk through them one
      // by one and change the layout as we find the layout config. This
      // way, layout configuration can live anywhere within the path and
      // we won't miss it.
      //
      // Also, this will allow overriding the layout in any time so we
      // can have different layouts for different routes.
      const paths = route.pathFromRoot;
      paths.forEach((path) => {

          // Check if there is a 'layout' data
          if ( path.routeConfig && path.routeConfig.data && path.routeConfig.data.layout )
          {
              // Set the layout
              this.layout = path.routeConfig.data.layout;
          }
      });
  }

}
