import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'layout-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  encapsulation: ViewEncapsulation.None,
  exportAs: 'layout-footer'
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
