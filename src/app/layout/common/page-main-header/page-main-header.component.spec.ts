import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMainHeaderComponent } from './page-main-header.component';

describe('PageMainHeaderComponent', () => {
  let component: PageMainHeaderComponent;
  let fixture: ComponentFixture<PageMainHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageMainHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMainHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
