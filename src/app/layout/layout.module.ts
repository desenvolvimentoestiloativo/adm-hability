import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { LayoutComponent } from "./layout.component";
import { EmptyLayoutModule } from "./layouts/empty/empty.module";
import { VerticalLayoutModule } from './layouts/vertical/vertical.module';

const modules = [
  EmptyLayoutModule,
  VerticalLayoutModule,
];

@NgModule({
  declarations: [
      LayoutComponent,
  ],
  imports     : [
      SharedModule,
      ...modules
  ],
  exports     : [
      ...modules
  ]
})
export class LayoutModule
{
}
