import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterGroupAdmsComponent } from './register-group-adms.component';

describe('RegisterGroupAdmsComponent', () => {
  let component: RegisterGroupAdmsComponent;
  let fixture: ComponentFixture<RegisterGroupAdmsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterGroupAdmsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterGroupAdmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
