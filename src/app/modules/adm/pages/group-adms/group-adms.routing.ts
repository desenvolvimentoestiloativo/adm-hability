import { Route } from '@angular/router';
import { ListGroupAdmsComponent } from './list-group-adms/list-group-adms.component';

export const listGroupRoutes: Route[] = [
    {
        path     : '',
        component: ListGroupAdmsComponent,
    }
];
