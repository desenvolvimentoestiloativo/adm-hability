import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { SharedModule } from "src/app/shared/shared.module";
import { ListGroupAdmsComponent } from './list-group-adms/list-group-adms.component';
import { listGroupRoutes } from "./group-adms.routing";
import { RegisterGroupAdmsComponent } from './register-group-adms/register-group-adms.component';

@NgModule({
  declarations: [
      ListGroupAdmsComponent,
      RegisterGroupAdmsComponent
  ],
  imports     : [
      RouterModule.forChild(listGroupRoutes),
      SharedModule
  ]
})
export class GroupAdmsModule
{
}
