import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Adm, UserDetails } from './group-adms.type';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GroupAdmsService
{

  private _urlApi = environment.altHability;
  private _adms: BehaviorSubject<any>;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(
    private _httpClient: HttpClient
  )
  {
    // Set the private defaults
    this._adms = new BehaviorSubject(null);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  /**
   * Getter for Adms
   */
  get adms$(): Observable<any>
  {
      return this._adms.asObservable();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Get all Adms
   */
  getAllAdms(): Observable<Adm[]>
  {
      return this._httpClient.get<Adm[]>(`${this._urlApi}/adm/listall`).pipe(
          tap((response: any) => {
              this._adms.next(response);
          })
      );
  }

  createGroupAdm(objectAdm: Adm): Observable<Adm[]>
  {
      return this._httpClient.post<Adm[]>(`${this._urlApi}/adm/create`, {
        ...objectAdm
      }).pipe(
          tap((response: any) => {
              this._adms.next(response);
          })
      );
  }

  createUserBackoffice(objectUser: UserDetails): Observable<UserDetails[]>
  {
      return this._httpClient.post<UserDetails[]>(`${this._urlApi}/users/create`, {
        ...objectUser
      }).pipe(
          tap((response: any) => {
              this._adms.next(response);
          })
      );
  }

  updateGroupAdm(objectAdm: Adm): Observable<Adm[]>
  {
      return this._httpClient.post<Adm[]>(`${this._urlApi}/adm/update`, {
        ...objectAdm
      }).pipe(
          tap((response: any) => {
              this._adms.next(response);
          })
      );
  }

}
