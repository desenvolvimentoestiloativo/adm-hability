export interface Adm
{
  _id?: string;
  name: string;
  listUserBackoffice: Array<string>;
  idAdm: string;
  createdAt?: Date;
  updatedAt?: Date;
  active: boolean;
}

export interface UserDetailsOthers{
  company: string;
}

export interface UserDetails {
  // tslint:disable-next-line: variable-name
  _id?: string;
  username: string;
  email: string;
  firstName: string;
  password: string;
  lastName?: string;
  active: boolean;
  profile: string;
  groupAdm: string;
  others: UserDetailsOthers | any;
  createdAt?: Date;
  updatedAt?: Date;
}
