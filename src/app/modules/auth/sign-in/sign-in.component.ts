import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'auth-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AuthSignInComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
