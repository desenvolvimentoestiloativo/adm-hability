import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthSignInComponent } from './sign-in.component';
import { authSignInRoutes } from './sign-in.routing';

@NgModule({
  declarations: [
      AuthSignInComponent
  ],
  imports     : [
      RouterModule.forChild(authSignInRoutes),
      SharedModule
  ]
})
export class AuthSignInModule
{
}
