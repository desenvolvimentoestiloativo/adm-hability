FROM node:12-slim
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install -g @angular/cli
RUN npm install
COPY . ./
RUN npm run build
EXPOSE 8080
CMD [ "node", "server.js" ]

#gcloud builds submit --tag gcr.io/hability-297100/adm-hability --timeout=2h15m5s --project=hability-297100 --account=desenvolvimentoestiloativo@gmail.com
